package oop_station;

import java.util.Date;

public class PrepaidCard extends Ticket implements ICard {

	public PrepaidCard(int value, Date issuedDate) {
		super(value, issuedDate);
	}

	public void add(int value) {
		this.value+=value;
	}
	
	public boolean deduct(int value) {
		if(this.value < value) {
			return false;
		} else {
			this.value -= value;
			return true;
		}
	}
	@Override
	public boolean isValid() {
		if(issuedDate.before(new Date()) && value >= 9000) {
			return true;
		} 
		return false;
	}
}
