package innerClass;

import java.awt.*;
import java.awt.event.*;
 
public class AWTCounter3ButtonsGetSource extends Frame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TextField tfCount;
	private Button btnCountUp, btnCountDown, btnReset;
	private int count = 0;
 
	// Constructor to setup the GUI components and event handlers
	public AWTCounter3ButtonsGetSource () {
		setLayout(new FlowLayout());
		add(new Label("Counter"));
		tfCount = new TextField("0", 10);
		tfCount.setEditable(false);
		add(tfCount);
 
		// Construct Buttons
		btnCountUp = new Button("Count Up");
		add(btnCountUp);
		btnCountDown = new Button("Count Down");
		add(btnCountDown);
      	btnReset = new Button("Reset");
      	add(btnReset);
 
      	// Allocate an instance of inner class BtnListener.
      	AllButtonsListener listener = new AllButtonsListener();
      	// Use the same listener instance to all the 3 Buttons.
      	btnCountUp.addActionListener(listener);
      	btnCountDown.addActionListener(listener);
      	btnReset.addActionListener(listener);
 
      	addWindowListener(new MyWindowListener());
      
      	setTitle("AWT Counter");
      	setSize(400, 100);
      	setVisible(true);
	}
 
	// The entry main method	
	public static void main(String[] args) {
		new AWTCounter3ButtonsGetSource();  // Let the constructor do the job
	}	
 
	/**
	 * AllButtonsListener is a named inner class used as ActionEvent listener for all the Buttons.
	 */
	private class AllButtonsListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent evt) {
			// Need to determine which button has fired the event.
			Button source = (Button)evt.getSource();
			// Get a reference of the source that has fired the event.
			// getSource() returns a java.lang.Object. Downcast back to Button.
			if (source == btnCountUp) {
				++count;
			} else if (source == btnCountDown) {
				--count;
			} else {
				count = 0;
			}
			tfCount.setText(count + "");
		}
	}
	private class MyWindowListener implements WindowListener {
		// Called back upon clicking close-window button
		@Override
		public void windowClosing(WindowEvent evt) {
			System.exit(0);  // Terminate the program
		}	

		// Not Used, BUT need to provide an empty body to compile.
		@Override public void windowOpened(WindowEvent evt) { }
		@Override public void windowClosed(WindowEvent evt) { }
		// For Debugging
		@Override public void windowIconified(WindowEvent evt) { System.out.println("Window Iconified"); }
		@Override public void windowDeiconified(WindowEvent evt) { System.out.println("Window Deiconified"); }
		@Override public void windowActivated(WindowEvent evt) { System.out.println("Window Activated"); }
		@Override public void windowDeactivated(WindowEvent evt) { System.out.println("Window Deactivated"); }
	}
}