package aims.media;

import aims.PlayException;

public interface Playable {
    public void play() throws PlayException;
}
