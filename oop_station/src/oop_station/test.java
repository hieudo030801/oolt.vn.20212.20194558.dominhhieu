package oop_station;

import java.util.Date;

public class test {

	public static void main(String[] args) {
		Line mLine = new Line();
		
		/// Test PrepaidCard
		System.out.println("############## PrepaidCard ##############");
		
//		giá trị của thẻ = 0
		PrepaidCard ppCard00 = new PrepaidCard(0, new Date());
		ppCard00.getInfo();
		System.out.print("Enter Gate A: -- ");
		mLine.A.enter(ppCard00);
		
//		giá trị của thẻ không đủ mức sàn
		PrepaidCard ppCard01 = new PrepaidCard(500, new Date());
		ppCard01.getInfo();
		
		System.out.print("Enter Gate A: -- ");
		mLine.A.enter(ppCard01);
				
		System.out.print("Exit Gate C: -- ");
		mLine.C.exit(ppCard01);
		
//		Giá trị của thẻ đủ
		ppCard01.add(50000);
		System.out.println("\nAfter adding 50000 to the balance:");
		ppCard01.getInfo();
		
		System.out.print("Enter Gate A: -- ");
		mLine.A.enter(ppCard01);
		System.out.print("Exit Gate C: -- ");
		mLine.C.exit(ppCard01);
		
		
		
		/// Test OneWayTicket
		System.out.println("\n############## OneWayTicket ##############");
		
		OneWayTicket owTicket01 = new OneWayTicket( 500, new Date());
		owTicket01.getInfo();
		
		System.out.print("Enter Gate A: -- ");
		mLine.A.enter(owTicket01);
				
		System.out.print("Exit Gate C: -- ");
		mLine.C.exit(owTicket01);
		owTicket01.getInfo();
			
//		Đã ở trong sân ga nên không thể vào cổng xuất phát
		System.out.print("Enter Gate A: -- ");
		mLine.A.enter(owTicket01);
		

	}

}
