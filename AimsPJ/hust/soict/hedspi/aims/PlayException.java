package aims;

public class PlayException extends Exception {

	public PlayException() {
		// TODO Auto-generated constructor stub
	}

	public PlayException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PlayException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	public PlayException(String message, Throwable cause) {

		super(message, cause);
		// TODO Auto-generated constructor stub
	}
	public PlayException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {

		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}