package OtherPJs.lab05.garbage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
public class GarbageCreator {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        File file = new File("D:\\Code\\OOP\\OtherPJs\\lab05\\sample-4mb-text-file.txt");
        BufferedReader br = new BufferedReader(new FileReader(file));
        String st;
        String s ="";
        while ((st = br.readLine()) != null)
            s += st;
        System.out.println(s);
        br.close();
    }
}