package OtherPJs.lab09.gui.awt;

import java.awt.*;
import java.awt.event.*;

public class AWTCounter extends Frame /*implements ActionListener*/ {
    // khai bao cac component tren giao dien 
    private Label lblCount;
    private TextField tfCount;
    private Button btnCount;
    private int count;

    public AWTCounter () {
        //Thiet lap layout cho cua so giao dien
        //FlowLayout: bo cuc dang dong gian: component nao
        //duoc them vao truoc se xuat hien truoc tren 1 hang
        //neu khong du cho thi xuong hang moi
        setLayout(new FlowLayout());
        
        //Khoi tao cac component va them vao giao dien
        lblCount = new Label("Counter"); // construct the Label component
        add(lblCount); // "super" Frame container adds Label component

        tfCount = new TextField(count + "", 10); // construct the TextField component with initial text
        tfCount.setEditable(false); // set to read-only

        add(tfCount); // "super" Frame container adds TextField component
        btnCount = new Button("Count"); // construct the Button component
        add(btnCount); // "super" Frame container adds Button component
        btnCount.addActionListener(
            //this
            new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    ++count;
                    tfCount.setText(count+"");
                }
            }
            );
        // "btnCount" is the source object that fires an ActionEvent when clicked.
        // The source add "this" instance as an ActionEvent listener, which provides
        // an ActionEvent handler called actionPerformed().
        // Clicking "btnCount" invokes actionPerformed().

        //Thiet lap thong so cho cua so giao dien
        setTitle("AWT Counter"); // "super" Frame sets its title
        setSize(250, 100); 
        setVisible(true); 
    }
    public static void main(String[] args){
        AWTCounter app = new AWTCounter();
    }
    /*@Override
    public void actionPerformed(ActionEvent evt) {
        ++count; // Increase the counter value
        // Display the counter value on the TextField tfCount
        tfCount.setText(count + ""); // Convert int to String
    }*/
}
