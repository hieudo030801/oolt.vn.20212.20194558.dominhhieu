package innerClass;

import java.awt.*;
import java.awt.event.*;

 
// An AWT GUI program inherits the top-level container java.awt.Frame
public class AWTCounter3Buttons1Listener extends Frame {
   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TextField tfCount;
	private Button btnCountUp, btnCountDown, btnReset;
	private int count = 0;
 
	// Constructor to setup the GUI components and event handlers
	public AWTCounter3Buttons1Listener () {
		setLayout(new FlowLayout());
		add(new Label("Counter"));
		tfCount = new TextField("0", 10);
		tfCount.setEditable(false);
		add(tfCount);
 
		// Construct Buttons
		btnCountUp = new Button("Count Up");
		add(btnCountUp);
		btnCountDown = new Button("Count Down");
		add(btnCountDown);
		btnReset = new Button("Reset");
		add(btnReset);
 
		// Allocate an instance of the "named" inner class BtnListener.
		AllButtonsListener listener = new AllButtonsListener();
		// Use the same listener instance for all the 3 Buttons.
		btnCountUp.addActionListener(listener);
		btnCountDown.addActionListener(listener);
		btnReset.addActionListener(listener);
		
		addWindowListener(new MyWindowListener());
		
		setTitle("AWT Counter");
		setSize(400, 100);
		setVisible(true);
	}
 
	// The entry main method
	public static void main(String[] args) {
		new AWTCounter3Buttons1Listener();  // Let the constructor do the job
	}	
 
	/**
	 * AllButtonsListener is an named inner class used as ActionEvent listener for all the Buttons.
	 */
	private class AllButtonsListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent evt) {
			// Need to determine which button fired the event.
			// the getActionCommand() returns the Button's label
			String btnLabel = evt.getActionCommand();
			if (btnLabel.equals("Count Up")) {
				++count;
			} else if (btnLabel.equals("Count Down")) {
				--count;
			} else {
				count = 0;
			}
			tfCount.setText(count + "");
		}
	}
	private class MyWindowListener implements WindowListener {
		// Called back upon clicking close-window button
		@Override
		public void windowClosing(WindowEvent evt) {
			System.exit(0);  // Terminate the program
		}	

		// Not Used, BUT need to provide an empty body to compile.
		@Override public void windowOpened(WindowEvent evt) { }
		@Override public void windowClosed(WindowEvent evt) { }
		// For Debugging
		@Override public void windowIconified(WindowEvent evt) { System.out.println("Window Iconified"); }
		@Override public void windowDeiconified(WindowEvent evt) { System.out.println("Window Deiconified"); }
		@Override public void windowActivated(WindowEvent evt) { System.out.println("Window Activated"); }
		@Override public void windowDeactivated(WindowEvent evt) { System.out.println("Window Deactivated"); }
	}
}