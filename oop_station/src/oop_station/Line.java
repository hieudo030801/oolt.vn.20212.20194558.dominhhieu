package oop_station;

public class Line {
	public final Gate A = new Gate("A", 0);
	public final Gate B = new Gate("B", 17);
	public final Gate C = new Gate("C", 22);
	public final Gate D = new Gate("D", 33);
	
	public Line() {
	}
	public static int getFare(int distance) {
		int cost =9000;
		int extra=(distance-6)/2;
		if((distance-6)%2!=0) extra++;
		cost +=extra*2000;
		return cost;
	}
}
