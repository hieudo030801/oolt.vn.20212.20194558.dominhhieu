package oop_station;

import java.util.Date;
public abstract class Ticket {
	protected Date issuedDate;
	protected int value;
	protected Gate gate=null;
	
	public Ticket (int value, Date date) {
		this.value=value;
		this.issuedDate=date;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public void setOrigin(Gate gate) {
		this.gate=gate;
	}
	
	public Gate getOrigin() {
		return this.gate;
	}
	
	public void adjustValue(int value) {
		this.value -= value;
	}
	
	public abstract boolean isValid();
	
	public void getInfo() {
		System.out.println("\n------------ Ticket Info ------------");
		System.out.println("issuedDate : " + this.issuedDate);
		System.out.println("value : " + this.getValue());
		System.out.println("-------------------------------------\n");
	}
}
